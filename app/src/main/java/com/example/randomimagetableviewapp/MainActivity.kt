package com.example.randomimagetableviewapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import com.example.randomimagetableviewapp.data.Datasource
import com.example.randomimagetableviewapp.model.RandomImage
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dataset : List<RandomImage> = Datasource().loadImages()
        val getRandomImageButton : Button = findViewById(R.id.get_random_image)
        val imageView : ImageView = findViewById(R.id.random_image)

        getRandomImageButton.setOnClickListener{
            imageView.setImageResource(dataset.random().imageResourceId)
            getRandomImageButton.setBackgroundColor(getRandomColor())
        }
    }
    fun getRandomColor() : Int {
        val rnd = Random
        return Color.argb(255,
            rnd.nextInt(256),
            rnd.nextInt(256),
            rnd.nextInt(256)
        )
    }
}