package com.example.randomimagetableviewapp.data

import com.example.randomimagetableviewapp.R
import com.example.randomimagetableviewapp.model.RandomImage

class Datasource{
    fun loadImages() :List<RandomImage>{
        return listOf<RandomImage>(
            RandomImage(R.drawable.cat),
            RandomImage(R.drawable.dog),
            RandomImage(R.drawable.code),
            RandomImage(R.drawable.ocean),
            RandomImage(R.drawable.toasts)
        )
    }
}