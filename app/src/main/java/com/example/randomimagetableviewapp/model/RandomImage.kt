package com.example.randomimagetableviewapp.model

import androidx.annotation.DrawableRes

class RandomImage(
    @DrawableRes val imageResourceId : Int
)
