## This app's implementation is a part of VibeLab Marathon
## Random Image App

### How app works:
<img src="randomimage.gif"  width="200" height="350t">


### App structure:
This app contains:

-model
-datasource
-main activity

![app structure](appstructure.jpeg)

model is used to provide image drawable resources

![model](model.jpeg)

datasource is needed to get models in MainActivity

![datasource](datasource.jpeg)

main activity is responsible for:

-drawing the ui
-listening to the button click
-changing the image and button's color

![main activity](mainActivity.jpg)

getRandomColor() - returns Int to be used as RGB color

![random color](randomcolor.jpg)